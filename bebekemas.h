#ifndef BEBEKEMAS_H
#define BEBEKEMAS_H

/********************************************************************************************************
 *                                ( Copyright : M.Sami.Shihab )                                         *
 *                                                                                                      *
 *      baris pertama : T => banyaknya toko yang bakal dikunjungin "<1000"                              *
 *      baris kedua : banyaknya bebek dalem satu toko (B) "<1000"                                       *
 *      baris ketiga dst : kualitas telur perbebek. ===> ex: 1 2 3 8 1 (ada 5 telor untuk bebek "b")    *
 *          ---> kualias = 0 < X < 10                                                                   *
 *                                                                                                      *
 *      Jumlah telor per-bebek adalah 3 karena dihitung berdasarkan 3 hari                              *
 ********************************************************************************************************
 *      contoh input:                                                                                   *
 *      Masukkan T (Jumlah toko)                        : "3"                                           *
 *      Masukkan B (Jumlah bebek per-toko)              : "2"                                           *
 *      ===========================================================                                     *
 *                                                                                                      *
 *      Masukkan kualtias telor Toko 1, Bebek 1         : "0 0 5"                                       *
 *      Masukkan kualitas telor Toko 1, Bebek 2         : "5 5 3"                                       *
 *      -----------------------------------------------------------                                     *
 *      Masukkan kualitas telor Toko 2, Bebek 1         : "9 9 9"                                       *
 *      Masukkan kualitas telor Toko 2, Bebek 2         : "0 2 1"                                       *
 *                                                                                                      *
 *      ===============< Hasil pencarian bebek emas>===============                                     *
 *      Toko #1 : Bebek ke-2                                                                            *
 *      Toko #2 : Bebek ke-2                                                                            *
 *                                                                                                      *
 ********************************************************************************************************/

#include <stddef.h> //for the ptrdiff_t type that will be used in looping of int Toko_t::telorEmas();
#include <vector>

enum limit_t {QUALITY_LIMIT = 10, BIG_LIMIT = 1000, MAX_TELOR=3};
/*
 * QUALITY_LIMIT    : The range of eggs quality X => 0 < X < 10
 * BIG_LIMIT        : Maximum number of Toko and Bebek. But since Toko could have 1000 Bebeks,
 *                    the max number of Bebeks is number of Toko * 1000
 * MAX_TELOR        : Can be said as the number of days observed, since the duck will only lay
 *                    one egg per-day
 */

struct Bebek_t{
    //There'll be 3 eggs per duck, with quality defined with 'int' and quality ranges from 0 to 10
    int telorQuality[MAX_TELOR];
    int sumTelorQuality(){
        return telorQuality[0] + telorQuality[1] + telorQuality[2]; //Jumlah telor selama 3 hari
    }
};

class Toko_t{
    size_t bSize;
    public:
        Toko_t(size_t s = 0){ bSize=s; Bebek=new Bebek_t(bSize); }
        Toko_t (const Toko_t&);
        virtual ~Toko_t() {delete[] Bebek;}

        void setTelor(const int, const int[]);
        int telorEmas();

    private:
        std::vector<Bebek_t> Bebek;
        int static jumlahBebek;
};

template <typename T>
T CheckRange(const T& jumlah)
{
    return (jumlah > 0 && jumlah<=BIG_LIMIT) ? jumlah:0;    //return 0 means the input is out of range
}


class BebekEmas
{
    public:
        BebekEmas();
        BebekEmas(int const, int const);
        virtual ~BebekEmas() {delete[] Toko;}

    private:
        int jumlahToko;
        int jumlahBebek;
        Toko_t *Toko;
};


#endif // BEBEKEMAS_H
