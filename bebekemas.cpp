#include "bebekemas.h"

//----------------------------------------- < TOKO CLASS > ------------------------------------------------
void Toko_t::setTelor(const int index, const int telorsInput[]){
    (Bebek+index)->telorQuality = telorsInput;
}

int Toko_t::telorEmas(){
    int max = Bebek->sumTelorQuality();
    int index=1;
    ptrdiff_t telorEmas=0;
    while( Bebek+index++ != nullptr)
    {
        if (Bebek->sumTelorQuality() > max)
        {
            max = Bebek->sumTelorQuality();
            telorEmas = index;
        }
    }

    return telorEmas;
}

//---------------------------------------- < BEBEK EMAS CLASS > -------------------------------------------
BebekEmas::BebekEmas()
{
    this->jumlahToko = jumlahBebek = 0;
//    this->jumlahBebek = 0;
    this->Toko = new Toko_t[jumlahToko];
}
BebekEmas::BebekEmas(const int jumlahToko, const int jumlahBebek){
    this->jumlahToko = CheckRange(jumlahToko);
    this->jumlahBebek = CheckRange(jumlahBebek);
//    Toko_t::jumlahBebek = jumlahBebek;
    this->Toko = new Toko_t[jumlahToko];
}
