TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    bebekemas.cpp

HEADERS += \
    bebekemas.h

QMAKE_CXXFLAGS += -std=c++0x
